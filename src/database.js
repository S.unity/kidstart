import axios from "axios";
let database = []
axios.get('/database.json').then(async res => await database.push(JSON.parse(res.data)))
export default {
    database
}